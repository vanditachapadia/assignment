let operation = <HTMLInputElement>document.getElementById('operations');
let resultdiv = <HTMLInputElement>document.getElementById('result-div');

function addChar(element) {
    let char = element.getAttribute('data-value');
    operation.innerText += char;
}

function cube(){
    let operationValue = Number(operation.innerText);
    operation.innerText = (operationValue * operationValue * operationValue).toString();
}
function errorfun(result){
    if(result === 'NaN'){
        operation.innerText = "Error";
    }else{
        operation.innerText = result;
    }
}

function clearScreen() {
    operation.innerText = "";
}


function backspace() {
    let operationValue = operation.innerText;
    let operationValueLength = operationValue.length;
    let newOperationValue = operationValue.substring(0, operationValueLength-1);
    operation.innerText = newOperationValue;
}

function calculate() {
    let operationValue = operation.innerText;
    operation.innerText = eval(operationValue);
}

// trigonometry function to calculate sine,cos,tan,cot,cosec,sec
function trigonometry(fun){
    let operationValue = Number(operation.innerText);
    let result;
    switch (fun) {
        case 'sine':
            result = Math.sin(operationValue);
            break;
        case 'cos':
            result = Math.cos(operationValue);
            break; 
        case 'tan':
            result = Math.tan(operationValue);
            break;
        case 'cot':
            result = 1 / Math.tan(operationValue);
            break;  
        case 'cosec':
            result = 1 / Math.sin(operationValue);
            break;
        case 'sec':
            result = 1 / Math.cos(operationValue);
            break;         
    }
    errorfun(result);
}

function func(fun){
    let operationValue = Number(operation.innerText);
    let result;
    switch (fun) {
        case 'random':
            result = (Math.random()*100);
            break;
        case 'ceil':
            result = Math.ceil(operationValue);
            break; 
        case 'floor':
            result = Math.floor(operationValue);
            break;
        case 'round':
            result = Math.round(operationValue);
            break;  
                
    }
    errorfun(result);
}



function square(){
    let operationValue = Number(operation.innerText);
    operation.innerText = (operationValue * operationValue).toString();
}

function absolute(){
    operation.innerText = (Math.abs(Number(operation.innerText))).toString();
}

function xp(){
    operation.innerText = (Math.exp(Number(operation.innerText))).toString();
}

function sqrt(){
    operation.innerText = (Math.sqrt(Number(operation.innerText))).toString();
}

function factorial(){
    let operationValue = operation.innerText;
    let result = 1;
    
    for(let i = Number(operationValue);i >= 1; i--){
        result *= i;
    }
    operation.innerText = (result).toString();
}

function ten_pow(){
    operation.innerText = (Math.pow(10,Number(operation.innerText))).toString();
}

declare interface Math {
    log10(x: number): number;
    }
// log base 10 function
function log(){
    operation.innerText = (Math.log10(Number(operation.innerText))).toString();
}

//natural logarithm function
function ln(){
    operation.innerText = (Math.log(Number(operation.innerText))).toString();
}

// MC, MR, M+, M-, MS funtionality
let memoryArr = [];

function cclear(){
    memoryArr = [];
    operation.innerText = ""; 
  
}

function recall(){
    let sum = 0;
    if(memoryArr.length===0){
        operation.innerText = "0";
    }
    else{
    for(let x of memoryArr){
        sum += Number(x);
    }
    operation.innerText = sum.toString();
}

}

function plus(){
    if(operation.innerText != ""){
        memoryArr.push(operation.innerText);
    }
    
    operation.innerText = ""; 
}

function minus(){
    if(operation.innerText != ""){
        memoryArr.push('-'+operation.innerText);
    }
   
    operation.innerText = "";
}

function save(){
    if(operation.innerText != ""){
        memoryArr.push(operation.innerText);
    }
    
    operation.innerText = "";
}