require("dotenv").config();
require("./config/database").connect();
const express = require("express");
const bcrypt =require("bcryptjs")
const jwt=require("jsonwebtoken")
const auth = require("./middleware/auth");
const helmet =require('helmet');
const cookieParser = require('cookie-parser')
const sessions = require('express-session');

const app = express();
const cors = require('cors');
app.use(cors({
  origin: '*'
}));
app.use(
  helmet.dnsPrefetchControl({
    allow: true,
  })
 );
 app.use(cookieParser());
 // creating 24 hours from milliseconds
const oneDay = 1000 * 60 * 60 * 24;

//session middleware
app.use(sessions({
    secret: "thisismysecrctekeyfhrgfgrfrty84fwir767",
    saveUninitialized:true,
    cookie: { maxAge: oneDay },
    resave: false
}));

app.use(express.json());
// importing user context
const User = require("./model/user");

app.post("/welcome", auth, (req, res) => {
    res.status(200).send("Welcome 🙌 ");
  });
  
app.post("/register", async (req, res) => {

    // Our register logic starts here
    session=req.session;
    if(session){
    try {
      // Get user input
      const { first_name, last_name, email, password } = req.body;
  
      // Validate user input
      if (!(email && password && first_name && last_name)) {
        res.status(400).send("All input is required");
      }
  
      // check if user already exist
      // Validate if user exist in our database
      const oldUser = await User.findOne({ email });
  
      if (oldUser) {
        return res.status(409).send("User Already Exist. Please Login");
      }
  
      //Encrypt user password
      encryptedPassword = await bcrypt.hash(password, 10);
  
      // Create user in our database
      const user = await User.create({
        first_name,
        last_name,
        email: email.toLowerCase(), // sanitize: convert email to lowercase
        password: encryptedPassword,
      });
  
      // Create token
      const token = jwt.sign(
        { user_id: user._id, email },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }
      );
      // save user token
      user.token = token;
  
      // return new user
      res.status(201).json(user);
    } catch (err) {
      console.log(err);
    }
  }
  else{
  res.status(408).send("Session timeout!!");
  }
    // Our register logic ends here
  });
  
  

  app.post("/login", async (req, res) => {

    // Our login logic starts here
    session=req.session;
    if(session){
    try {
      // Get user input
      const { email, password } = req.body;
  
      // Validate user input
      if (!(email && password)) {
        res.status(400).send("All input is required");
      }
      // Validate if user exist in our database
      const user = await User.findOne({ email });
  
      if (user && (await bcrypt.compare(password, user.password))) {
        // Create token
        const token = jwt.sign(
          { user_id: user._id, email },
          process.env.TOKEN_KEY,
          {
            expiresIn: "2h",
          }
        );
  
        // save user token
        user.token = token;
  
        // user
        res.status(200).json(user);
      }
      res.status(400).send("Invalid Credentials");
    } catch (err) {
      console.log(err);
    }
  }
  else{
  res.status(408).send("Session timeout!!");
  }
    // Our register logic ends here
  });

  app.get('/setcookie', (req, res) => {
    res.cookie(`Cookie token name`,`encrypted cookie string Value`,{
        maxAge: 5000,
        // expires works the same as the maxAge
        expires: new Date('01 12 2021'),
        secure: true,
        httpOnly: true,
        sameSite: 'lax'
    });
    res.send('Cookie have been saved successfully');
});

module.exports = app;